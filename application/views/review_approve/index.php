<div class="white-area-content">
<p>To verify your status as an accredited investor, you will need to submit your information to VerifyInvestor. The link below launch their site where you will need to provide additional information. You will be redirected back to this site when you're done.</p>
<form id="verifyInvestorLaunch">
<p><em>An accredited investor, in the context of a natural person, includes anyone who: earned income that exceeded $200,000 (or $300,000 together with a spouse) in each of the prior two years, and reasonably expects the same for the current year, or has a net worth over $1 million, either alone or together with a spouse (excluding the value of the person’s primary residence).</em>
    <input type="hidden" id="username" name="username" value="<?php echo $this->user->info->username ?>">
    <input type="hidden" id="email" name="email" value="<?php echo $this->user->info->email ?>" required>
    <input type="hidden" id="first_name" name="first_name" value="<?php echo $this->user->info->first_name ?>" required>
    <input type="hidden" id="last_name" name="last_name" value="<?php echo $this->user->info->last_name ?>" required>
  </p>
  <p><input id="launchVerifyInvestor" type="submit" class="btn btn-success" value="Launch Verify Investor" /></p>
</form>
<hr>
</div>