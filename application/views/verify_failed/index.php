<ol class="breadcrumb">
  <li>Review Status: Approved</li>
  <li>Pending Verification</li>
  <li class="active">Verification Status: Unable to Verify</li>
</ol>
<div class="white-area-content">

<div class="db-header clearfix">
    <div class="page-header-title">Verify Investor</div>
    <div class="db-header-extra"></div>
</div>

<h1>Your investor status was not verified.</h1>
<p>***COPY NEEDED*** Unfortunately we were unable to verify your status at this time. Contact us if you have any questions regarding this status.</p>
<hr>
</div>