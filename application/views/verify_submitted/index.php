<?php
  if (isset($_POST['vi_user_id']) || isset($this->ceresusers->vi_user_id)) {
      $data = array("vi_user_id" => $_POST['vi_user_id']);
      $this->db->where("ID",$this->user->info->ID);
      $this->db->update("ceresusers",$data); 
  }
?>

<div class="white-area-content">
  <div class="db-header clearfix">
    <div class="page-header-title">CERES Coin Investor Portal: Verify Investor Submitted</div>
  </div>
  <p>Thank you for submitting your information. We will notify you once we have verified your status.</p>
  <p><a href="<?php echo site_url(); ?>" class="btn btn-success">Return Home</a></p>
</div>
