<div class="white-area-content">
<div class="db-header clearfix">
    <div class="page-header-title">CERES Coin: Verified</div>
    <div class="db-header-extra"></div>
</div>

<ol class="breadcrumb">
  <li>Submit Information</li>
  <li>Pending Review</li>
  <li>Review Status: Approved</li>
  <li>Pending Verification</li>
  <li class="active">Verification Status: Verified</li>
  <li>Submit Payment</li>
</ol>
<h1>Your investor status has been verified.</h1>
<p>***COPY NEEDED*** Good News! You are now able to invest in CERES Coin. Select your payment type below:</p>
<hr>
  <div class="row">
    <div class="col-sm-6"><a id="launchACHPayment" href="<?php echo site_url("submit_ach"); ?>" class="btn btn-default" value="Invest via ACH" />Pay via ACH</a></div>
    <div class="col-sm-6"><a id="launchCryptoPayment" href="<?php echo site_url("submit_crypto"); ?>" class="btn btn-default" value="Invest via Cryptocurrency" />Pay via Cryptocurrency</a></div>
  </div>
<hr>
</div>