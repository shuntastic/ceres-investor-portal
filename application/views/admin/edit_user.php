<div class="white-area-content">
<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
    <div class="db-header-extra"></div>
</div>
<p>Here you can send messages to the user or change their status in the process.</p>
<div class="panel panel-default">
<div class="panel-body">
<?php echo form_open_multipart(site_url("admin/edit_member_pro/" . $member->ID), array("class" => "form-horizontal")) ?>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group user-info">
            <h2><?php echo $member->first_name.' '.$member->last_name; ?></h2>
            <h3><?php echo $member->email; ?></h3>
            <p><?php echo $member->username; ?></p>
            <p>Joined: <em><?php echo date($this->settings->info->date_format, $member->joined) ?></em></p>
            <p>Last Online: <em><?php echo date($this->settings->info->date_format, $member->online_timestamp) ?></em></p>
            <p>Account Activated: <em><?php echo (($member->active) ? 'Yes' : 'No'); ?></em></p>
            <hr />
            <p><?php echo $member->address_1; ?></p>
            <p><?php echo $member->address_2; ?></p>
            <p><?php echo $member->city ?>, <?php echo $member->state ?> <?php echo $member->zipcode ?></p>
            <p><?php echo $member->country ?></p>
            <p><a href="<?php echo site_url("admin/email_member/" . $member->username) ?>" class="btn btn-info btn-sm">Send Message</a></p>
            <?php if(($member->user_role == 7) || ($member->user_role == 10) || ($member->user_role == 13)) { ?>
                <p><a href="<?php echo site_url("verify_submitted/check_status/" . $member->ID) ?>" class="btn btn-info btn-sm">Check Investor Status</a></p>
            <?php } ?>
            <input type="hidden" value="<?php  echo (($member->active) ? 1 : 0); ?>">
            <input type="hidden" class="form-control" id="email-in" name="email" value="<?php echo $member->email ?>">
            <input type="hidden" class="form-control" id="username" name="username" value="<?php echo $member->username ?>">
            <input type="hidden" class="form-control" id="password-in" name="password" value="">
            <input type="hidden" class="form-control" id="name-in" name="first_name" value="<?php echo $member->first_name ?>">
            <input type="hidden" class="form-control" id="name-in" name="last_name" value="<?php echo $member->last_name ?>">
            <input type="hidden" class="form-control" id="name-in" name="credits" value="<?php echo $member->points ?>">
            <textarea class="form-control" style="display:none;" name="aboutme" rows="8"><?php echo nl2br($member->aboutme) ?></textarea>
            <input type="hidden" name="address_1" class="form-control" value="<?php echo $member->address_1 ?>">
            <input type="hidden" name="address_2" class="form-control" value="<?php echo $member->address_2 ?>">
            <input type="hidden" name="city" class="form-control" value="<?php echo $member->city ?>">
            <input type="hidden" name="state" class="form-control" value="<?php echo $member->state ?>">
            <input type="hidden" name="zipcode" class="form-control" value="<?php echo $member->zipcode ?>">
            <input type="hidden" name="country" class="form-control" value="<?php echo $member->country ?>">
            <span style="display:none;"><img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $member->avatar ?>" />
            <input type="file" name="userfile" /></span>

        </div>
    </div>
    <?php if($member->user_role != 1) { ?>
        <div class="col-sm-6">
            <div class="form-group user-status">
            <h4>Set User Status</h4>
            <p>Set the user's status based on where they are in the investor process</p>
                <div class="radio-group">
                    <input type="radio" value="5" id="role5" name="user_role" <?php if($member->user_role == 5) echo "checked" ?>><label for="role5">New User</label>
                    <input type="radio" value="9" id="role9" name="user_role" <?php if($member->user_role == 9) echo "checked" ?>><label for="role9">Review: Pending</label>
                    <input type="radio" value="10" id="role10" name="user_role" <?php if($member->user_role == 10) echo "checked" ?>><label for="role10">Review: Approve (email will be sent)</label>
                    <input type="radio" value="11" id="role11" name="user_role" <?php if($member->user_role == 11) echo "checked" ?>><label for="role11">Review: Rejected</label>
                    <input type="radio" value="7" id="role7" name="user_role" <?php if($member->user_role == 7) echo "checked" ?>><label for="role7">Verify Investor: Pending</label>
                    <input type="radio" value="8" id="role8" name="user_role" <?php if($member->user_role == 8) echo "checked" ?>><label for="role8">Verify Investor: Verified</label>
                    <input type="radio" value="13" id="role13" name="user_role" <?php if($member->user_role == 13) echo "checked" ?>><label for="role13">Verify Investor: Failed</label>
                    <input type="radio" value="14" id="role14" name="user_role" <?php if($member->user_role == 14) echo "checked" ?>><label for="role14">Investment: Pending</label>
                    <input type="radio" value="12" id="role12" name="user_role" <?php if($member->user_role == 12) echo "checked" ?>><label for="role12">Invested</label>
                    <input type="radio" value="6" id="role6" name="user_role" <?php if($member->user_role == 6) echo "checked" ?>><label for="role6">Ban User</label>
                </div>
            </div>
        </div>
    <?php } ?>

</div>
    <?php if($member->user_role != 1) { ?>
    <p>
        <input type="submit" class="col-md-3 btn btn-primary form-control" value="<?php echo lang("ctn_13") ?>" />
    </p>
    <?php } ?>
<?php echo form_close() ?>
</div>
</div>
</div>