<div class="white-area-content">
<div class="db-header clearfix">
    <div class="page-header-title">CERES Coin: Invest via ACH</div>
    <div class="db-header-extra"></div>
</div>
<h1>Submit your ACH Payment</h1>
<p>Please fill in the form below with your basic banking information, we will provide you with need to complete the transaction via ACH.</p>
<hr>
  <div class="row">
    <div class="col-sm-12">
    <?php echo form_open_multipart(site_url("submit_ach/submit_payment"), array("class" => "form-horizontal","id" => "submitACH")) ?>

    <fieldset>

<!-- Form Name -->
<!-- <legend>Submit via ACH</legend> -->
<div class="form-group">
  <label class="col-md-4 control-label" for="amount">Amount (USD)</label>
  <div class="col-md-4">
    <div class="input-group">
      <span class="input-group-addon">$</span>
      <input id="amount" name="amount" class="form-control" placeholder="50000" type="text" required="">
    </div>
    <p class="help-block">mininum amount: $50,000</p>
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="bankname">Financial Institution Name</label>  
  <div class="col-md-4">
  <input id="bankname" name="bankname" type="text" placeholder="" class="form-control input-md" required="Please provide your Bank's Name">
    
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="routingnumber">Routing Number</label>  
  <div class="col-md-4">
  <input id="routingnumber" name="routingnumber" type="text" placeholder="" class="form-control input-md" required="Please provide your routing number">
    
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="sendPaymentACH"></label>
  <div class="col-md-4">
    <button id="sendPaymentACH" name="sendPaymentACH" class="btn btn-primary">Submit</button>
  </div>
</div>

</fieldset>
<?php echo form_close(); ?>		

    </div>
  </div>
<hr>

</div>