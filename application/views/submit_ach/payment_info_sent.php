<div class="white-area-content">
<div class="db-header clearfix">
	<div class="page-header-title">CERES Coin: Payment Information Sent</div>
</div>
<hr>
<div class="clearfix">
  <div class="row">
    <div class="col-sm-12">
      <p>Thank you for submitting your banking information.<br /> Use the banking information below to complete the ACH transfer.<br /><br />
          <b>Send Payment to:</b><br />
          <b>Bank:</b> Chase Bank<br />
          <b>ABA Routing #:</b>  111000614<br />
          <b>Account Number:</b>  250739130<br />
          <b>Account Name:</b>CERES COIN LLC<br />
          Transaction ID (include this number in the memo area):<?php echo $refid; ?><br />
      </p>
      <p><a href="<?php echo site_url(); ?>" class="btn btn-success">Return Home</a></p>
    </div>
</div>
</div>
</div>