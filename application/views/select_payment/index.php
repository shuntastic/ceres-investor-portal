<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>">Home</a></li>
  <li class="active">Verify Investor</li>
</ol>
<div class="white-area-content">

<div class="db-header clearfix">
    <div class="page-header-title">Verify Investor</div>
    <div class="db-header-extra"></div>
</div>

<h1>Thank you for submitting your information</h1>
<p>We will notify you once we have verified your status.</p>
<p><input id="checkVerifyStatus" type="button" class="btn btn-default" data-username="<?php echo $this->user->info->username; ?>" value="Check your Investor Verify Status" /></p>
<hr>
<div class="status">
<?php

if (isset($_GET['vi_user_id']) || isset($this->ceresusers->vi_user_id)) {
  // $apiURL = 'https://verifyinvestor-api-staging.herokuapp.com/api/v1/users/:'.$_GET['vi_user_id'].'/verification_requests';
  
    $data = array("vi_user_id" => $_GET['vi_user_id']);
    $this->db->where("ID",$this->user->info->ID);
    $this->db->update("ceresusers",$data); 
  
    $apiURL = 'https://verifyinvestor-api-staging.herokuapp.com/api/v1/users/:'.$_GET['vi_user_id'].'/verification_requests';
  
    echo $_GET['vi_user_id'];
  } else {
    $apiURL = 'https://verifyinvestor-api-staging.herokuapp.com/api/v1/users';

  }
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_HTTPHEADER, Array("Authorization: Token 2d55995b8c97af4262b67cc50aa2e1a48cd1993b7dba501f32","Content-Type: application/json","Content-Accept: application/json"));
    curl_setopt($ch, CURLOPT_URL,$apiURL);
    $result=curl_exec($ch);
    curl_close($ch);
    var_dump(json_decode($result, true));
    
    
		// echo '<div>'.verifyinvestor_check_status().'</div>'; 

?>
</div>
</div>