<div class="white-area-content">
<div class="db-header clearfix">
    <div class="page-header-title">CERES Coin: Invest via Cryptocurrency</div>
    <div class="db-header-extra"></div>
</div>
<h1>Submit your Crypto Payment</h1>
<p>Please fill in the form below to submit your payment using cryptocurrency.</p>
<hr>
  <div class="row">
    <div class="col-sm-12">
    <?php echo form_open_multipart(site_url("submit_crypto/submit_payment"), array("class" => "form-horizontal","id" => "submitCrypto")) ?>

    <fieldset>
    <input type="hidden" id="username" name="username" value="<?php echo $this->user->info->username ?>">
    <input type="hidden" id="email" name="email" value="<?php echo $this->user->info->email ?>">
    <input type="hidden" id="buyer_email" name="buyer_email" value="<?php echo $this->user->info->email ?>">
    <input type="hidden" id="buyer_name" name="first_name" value="<?php echo $this->user->info->first_name.' '.$this->user->info->last_name; ?>">
    <input type="hidden" id="item_name" name="item_name" value="CERES Raise">
    <input type="hidden" id="ipn_url" name="ipn_url" value="<?php echo site_url("submit_ach_ipn"); ?>">

<!-- <legend>Submit via Crypto</legend> -->
<div class="form-group">
  <label class="col-md-4 control-label" for="amount">Amount (USD)</label>
  <div class="col-md-4">
    <div class="input-group">
      <span class="input-group-addon">$</span>
      <input id="amount" name="amount" class="form-control" placeholder="50000" type="text" required="">
    </div>
    <p class="help-block">mininum amount: $50,000</p>
  </div>
</div>
<input type="hidden" id="currency1" name="currency1" value="USD">
<div class="form-group">
  <label class="col-md-4 control-label" for="currency2">Crypto Type</label>
  <div class="col-md-4">
    <select id="currency2" name="currency2" class="form-control">
      <option value="BTC">Bitcoin</option>
      <option value="LTC">Litecoin</option>
      <option value="BCH">Bitcoin Cash</option>
      <option value="ETH">Ether</option>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="sendPaymentCrypto"></label>
  <div class="col-md-4">
    <button id="sendPaymentCrypto" name="sendPaymentCrypto" class="btn btn-primary">Submit</button>
  </div>
</div>

</fieldset>
<?php echo form_close(); ?>		
    </div>
  </div>
<hr>
<div id="cryptoStatus">
<?php if(isset($outdata))  {
  echo $outdata;
}
?>
</div>
</div>