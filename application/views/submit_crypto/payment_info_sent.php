<div class="white-area-content">
<div class="db-header clearfix">
	<div class="page-header-title">CERES Coin: Payment Information Sent</div>
</div>
<hr>
<div class="clearfix">
  <div class="row">
    <div class="col-sm-12">
    <p>Thank you for submitting your crypto payment.<br />
    Complete your cryptocurrency transaction by clicking here:<br /><a href="<?php echo $status_url; ?>" target="_blank"><?php echo $status_url; ?></a><br /><br />
    Your reference number for the transaction.<br /><br />
    Transaction ID: <?php echo $refid; ?></p>
    <p><a href="<?php echo site_url(); ?>" class="btn btn-primary">Return Home</a></p>
   </div>
</div>
</div>
</div>
