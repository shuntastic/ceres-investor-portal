<!-- <ol class="breadcrumb">
	<li class="active">Submit Information</li>
  	<li>Pending Review</li>
</ol> -->
<div class="white-area-content">
<div class="db-header clearfix">
	<div class="page-header-title">CERES Coin Investor Portal: Submit Information</div>
</div>
<p>Please enter your information below so we may begin your review process.</p>
<hr>
<div class="row">
    <div class="col-sm-11">
<?php echo form_open_multipart(site_url("user_settings/pro"), array("class" => "form-horizontal","id" => "preregister")) ?>
	<input type="hidden" class="form-control" id="username" name="username" value="<?php echo $this->user->info->username ?>">
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_230") ?></label>
	    <div class="col-sm-10">
	      <input type="email" class="form-control" id="email" name="email" value="<?php echo $this->user->info->email ?>" required>
	    </div>
			<div class="col-sm-10 messages"></div>
	</div>
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_231") ?></label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $this->user->info->first_name ?>" required>
	    </div>
			<div class="col-sm-10 messages"></div>
	</div>
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_232") ?></label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $this->user->info->last_name ?>" required>
	    </div>
			<div class="col-sm-10 messages"></div>
	</div>
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_391") ?></label>
	    <div class="col-sm-10">
	      <input type="text" name="address_1" class="form-control" value="<?php echo $this->user->info->address_1 ?>" required>
	    </div>
			<div class="col-sm-10 messages"></div>
	</div>
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_392") ?></label>
	    <div class="col-sm-10">
	      <input type="text" name="address_2" class="form-control" value="<?php echo $this->user->info->address_2 ?>">
	    </div>
			<div class="col-sm-10 messages"></div>
	</div>
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_393") ?></label>
	    <div class="col-sm-10">
	      <input type="text" name="city" class="form-control" value="<?php echo $this->user->info->city ?>" required>
	    </div>
			<div class="col-sm-10 messages"></div>
	</div>
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_394") ?> </label>
	    <div class="col-sm-10">
	      <input type="text" name="state" class="form-control" value="<?php echo $this->user->info->state ?>" required>
	    </div>
			<div class="col-sm-10 messages"></div>
	</div>
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_395") ?></label>
	    <div class="col-sm-10">
	      <input type="text" name="zipcode" class="form-control" value="<?php echo $this->user->info->zipcode ?>" required>
	    </div>
			<div class="col-sm-10 messages"></div>
	</div>
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_396") ?></label>
	    <div class="col-sm-10">
	      <input type="text" name="country" class="form-control" value="<?php echo $this->user->info->country ?>" required>
	    </div>
			<div class="col-sm-10 messages"></div>
	</div>
	<div class="form-group">
		<div class="col-sm-1 col-sm-offset-2"><input type="checkbox" name="accinvestor" required></div>
		<div class="col-sm-9"><label for="accinvestor" class="control-label">I meet the requirements of an accredited investor*</label></div>
		<div class="col-sm-10 col-sm-offset-2"><em>*An accredited investor, in the context of a natural person, includes anyone who: earned income that exceeded $200,000 (or $300,000 together with a spouse) in each of the prior two years, and reasonably expects the same for the current year, or has a net worth over $1 million, either alone or together with a spouse (excluding the value of the person’s primary residence).</em></div>
	</div>
	<!-- <hr> -->
	<div class="col-sm-2 col-sm-offset-2"><input type="submit" name="s" value="Submit" class="btn btn-primary form-control" /></div>
	<?php echo form_close(); ?>		
		</div>
  </div>
<!-- <hr> -->

</div>
