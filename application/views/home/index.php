<div class="white-area-content">
<div class="db-header clearfix">
	<div class="page-header-title">Welcome to the CERES Coin Investor Portal</div>
</div>

<?php
	if($this->user->info->user_role == 1)	// Admin
	{ 
		redirect(base_url("admin/members"));

	} elseif($this->user->info->user_role == 5)	// Member - Just signed up
	{ 
?>
		<div class="clearfix">
			<p>Thank you for your interest in investing with CERES Coin. There will be a few steps necessary for you to complete before beginning to invest in CERES Coin:</p>
			<ol class="invstatus">
			<li class="active">Complete profile for review</li>
			<li>Verify your investor status</li>
			<li>Submit Funds</li>
			</ol>
		</div>
		<div class="clearfix">
		<p>Click here when you're ready to begin:</p>
		<p><a id="completeProfile" href="<?php echo site_url("submit_information"); ?>" class="btn btn-primary" />Complete Profile</a></p>
		</div>
<?php
		//redirect(base_url() . 'submit_information');
	} elseif($this->user->info->user_role == 6)	// Banned
	{
		redirect(base_url());
	} elseif($this->user->info->user_role == 7)	// Pending Verification
	{
?>
	<div class="clearfix">
		<p>Welcome back. We are currently awaiting confirmation of your investor status. We will follow up once we've received this information.</p>
		<ol class="invstatus">
		<li>Complete profile for review</li>
		<li class="active">Verify your investor status&nbsp;<span class="label label-success">PENDING</span></li>
		<li>Submit Funds</li>
		</ol>
	</div>
<?php
		// redirect(base_url() . 'verify_submitted');
	} elseif($this->user->info->user_role == 14)	// Pending Payment (Awaiting transaction from user)
	{
	?>
	<div class="clearfix">
		<p>Your status will be set to investor once your transaction has cleared.</p>
		<ol class="invstatus">
			<li>Complete profile for review&nbsp;<span class="label label-success">APPROVED</span></li>
			<li>Verify your investor status&nbsp;<span class="label label-success">VERIFIED</span></li>
			<li class="active">Submit Funds&nbsp;<span class="label label-warning">PENDING</span></li>
		</ol>
		<hr>
		  <div class="row">
			<div class="col-sm-11 col-sm-offset-1"><a id="launchACHPayment" href="<?php echo site_url("submit_ach"); ?>" class="btn btn-primary" value="Invest via ACH" />Pay via ACH</a>&nbsp;&nbsp;<a id="launchCryptoPayment" href="<?php echo site_url("submit_crypto"); ?>" class="btn btn-primary" value="Invest via Cryptocurrency" />Pay via Cryptocurrency</a></div>
		  </div>

	</div>
	<?php

	} elseif($this->user->info->user_role == 8)	// Verified
	{
?>
	<div class="clearfix">
		<p>Your investor status has been verified, and you are now eligible to invest in CERES Coin. Please select a form of payment:</p>
		<ol class="invstatus">
			<li>Complete profile for review&nbsp;<span class="label label-success">APPROVED</span></li>
			<li>Verify your investor status&nbsp;<span class="label label-success">VERIFIED</span></li>
			<li class="active">Submit Funds</li>
		</ol>
		<hr>
		  <div class="row">
			<div class="col-sm-11 col-sm-offset-1"><a id="launchACHPayment" href="<?php echo site_url("submit_ach"); ?>" class="btn btn-primary" value="Invest via ACH" />Pay via ACH</a>&nbsp;&nbsp;<a id="launchCryptoPayment" href="<?php echo site_url("submit_crypto"); ?>" class="btn btn-primary" value="Invest via Cryptocurrency" />Pay via Cryptocurrency</a></div>
		  </div>

	</div>
	<?php
	} elseif($this->user->info->user_role == 9)	// Pending Review
	{
	?>

	<div class="clearfix">
		<p>Your profile is currently under review. We will contact you shortly with your status.</p>
		<ol class="invstatus">
		<li class="active">Complete profile for review&nbsp;<span class="label label-success">SUBMITTED</span></li>
		<li>Verify your investor status</li>
		<li>Submit Funds</li>
		</ol>
	</div>
	
		<?php
			// redirect(base_url() . 'pending_review');
		} elseif($this->user->info->user_role == 10)	// Review Approve
		{
	?>
			<p>Your profile has been approved to proceed. We will now need to verify your investor status, click below to continue.</p>
		<ol class="invstatus">
			<li>Complete profile for review&nbsp;<span class="label label-success">APPROVED</span></li>
			<li class="active">Verify your investor status</li>
			<li>Submit Funds</li>
		</ol>
		<hr>
		  <div class="row">
			<div class="col-sm-5 col-sm-offset-1"><a href="<?php echo site_url("review_approve"); ?>" class="btn btn-primary" />Continue</a></div>
		  </div>
		<?php	
			// redirect(base_url() . 'review_approve');
	} elseif($this->user->info->user_role == 11)	// Review Rejected
		{ ?>
			<div class="clearfix">
				<p>We have reviewed your information and unfortunately are not able to let you invest at this time. <a href="mailto:info@ceresoin.io">Contact us</a> for more information.</p>
				<ol class="invstatus">
				<li class="active">Complete profile for review&nbsp;<span class="label label-danger">REJECTED</span></li>
				<li>Verify your investor status</li>
				<li>Submit Funds</li>
				</ol>
			</div>
	<?php 
			//redirect(base_url() . 'verify_submitted');
		} elseif($this->user->info->user_role == 12)	// Invested
		{
		?>
			<div class="clearfix">
				<p>You have successly invested with CERES Coin. Check back here for any updates.</p>
				<ol class="invstatus">
					<li>Complete profile for review&nbsp;<span class="label label-success">APPROVED</span></li>
					<li>Verify your investor status&nbsp;<span class="label label-success">VERIFIED</span></li>
					<li>Submit Funds&nbsp;<span class="label label-success">SUBMITTED</span></li>
				</ol>
			</div>
			
		<?php
		// redirect(base_url() . 'invested');
		} elseif($this->user->info->user_role == 13)	// Verify Failed
		{
		?>
		<div class="clearfix">
			<p>Unfortunately we were unable to verify your status at this time. <a href="mailto:info@ceresoin.io">Contact us</a> if you have any questions regarding this status.</p>
			<ol class="invstatus">
			<li>Complete profile for review&nbsp;<span class="label label-success">APPROVED</span></li>
			<li class="active">Verify your investor status&nbsp;<span class="label label-danger">REJECTED</span></li>
			<li>Submit Funds</li>
			</ol>
		</div>
	<?php
	}
?>
<hr>
<a href="mailto:info@ceresoin.io" class="btn btn-info btn-sm">Send Email to Admin</a>
</div>
