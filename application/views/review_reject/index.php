<ol class="breadcrumb">
  <li>Submit Information</li>
  <li>Pending Review</li>
  <li class="active">Review Status: Rejected</li>
  <!-- <li class="active alert alert-danger">Review Status: Rejected</li> -->
</ol>
<div class="white-area-content">
<div class="db-header clearfix">
    <div class="page-header-title">CERES Coin: Review Rejected</div>
    <div class="db-header-extra"></div>
</div>
<h1>Your registration was not accepted.</h1>
<p>We are unable to move forward at this time. Thank you for your interest.</p>
<hr>
</div>