<?php
/*
	CoinPayments.net
*/
	require('./coinpayments.inc.php');
	$cps = new CoinPaymentsAPI();
	$cps->Setup('9834bceF0084994bf9CfC33ECad3DE0D3DA082d6E904c36eD0b111e42f52404d', '96e44f63ab032a77c60f21b1178c38f53c5028e849960a3d06f8d4033431368e');
    if(
        isset($_POST['amount']) &&
        isset($_POST['currency1']) &&
        isset($_POST['currency2'])
    )
	$req = array(
		'cmd' => 'create_transaction',
		'amount' => $_POST['amount'],
		'currency1' => $_POST['currency1'],
		'currency2' => $_POST['currency2'],
		'address' => '', // leave blank send to follow your settings on the Coin Settings page
		'item_name' => 'CERES Raise',
		'ipn_url' => 'https://investor.ceres.local/ipn_handler',
	);
	// See https://www.coinpayments.net/apidoc-create-transaction for all of the available fields
			
	$result = $cps->CreateTransaction($req);
	if ($result['error'] == 'ok') {
		$le = php_sapi_name() == 'cli' ? "\n" : '<br />';
		print 'Transaction created with ID: '.$result['result']['txn_id'].$le;
		print 'Buyer should send '.sprintf('%.08f', $result['result']['amount']).' BTC'.$le;
		print 'Status URL: '.$result['result']['status_url'].$le;
	} else {
		print 'Error: '.$result['error']."\n";
	}
?>