<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Review_account extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("home_model");
		$this->load->model("user_model");
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		// If the user does not have premium. 
		// -1 means they have unlimited premium
		if($this->settings->info->global_premium && 
			($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) ) {
			$this->session->set_flashdata("globalmsg", lang("success_29"));
			redirect(site_url("funds/plans"));
		}
	}
	public function review($uid){
		$this->uid = $uid;
		// Loads HTML page
		$this->template->loadContent("review_account/review.php", array(
			)
		);
		
	}
	public function index() 
	{
		// if($this->user->info->user_role == 5) {

		// 	$regdate = date("Y-m-d H:i:s");
		// 	// $regdate = new DateTime();

		// 	$data = array("user_role" => 9);
		// 	$this->db->where("ID",$this->user->info->ID);
		// 	$this->db->update("users",$data); 

		// 	$data = array(				
		// 		'user_id' => $this->user->info->ID,
		// 		'email' => $this->user->info->email,
		// 		'first_name' => $this->user->info->first_name,
		// 		'last_name' => $this->user->info->last_name,
		// 		'street_address' => $this->user->info->address_1,
		// 		'apt_address' => $this->user->info->address_2,
		// 		'city' => $this->user->info->city,
		// 		'state' => $this->user->info->state,
		// 		'zipcode' => $this->user->info->zipcode,
		// 		'country' => $this->user->info->country,
		// 		'create_ts' => $regdate
		// 	);
		// 	$str = $this->db->insert('ceresusers', $data);
		// 	// $insert_id = $this->db->insert_id();

		// 	// $data = array(				
		// 	// 			'user_id' => $insert_id,
		// 	// 			'val_submit_ts'  => $regdate,
		// 	// 			'status' => 1,
		// 	// 			'val_source' => 'VerifyInvestor'
		// 	// 		);
		// 	// $str = $this->db->insert('investors', $data);

		// 	if(!isset($_COOKIE['language'])) {
		// 		// Get first language in list as default
		// 		$lang = $this->config->item("language");
		// 	} else {
		// 		$lang = $this->common->nohtml($_COOKIE["language"]);
		// 	}

		// 	// Send Email
		// 	$email_template = $this->home_model
		// 		->get_email_template_hook("new_registration", $lang);
		// 	if($email_template->num_rows() == 0) {
		// 		$this->template->error(lang("error_48"));
		// 	}
		// 	$email_template = $email_template->row();

		// 	$email_template->message = $this->common->replace_keywords(array(
		// 		"[NAME]" => $this->user->info->first_name.' '.$this->user->info->last_name,
		// 		"[EMAIL]" => $this->user->info->email,
		// 		"[SITE_URL]" => 
		// 			site_url("review_account/" . $this->user->info->ID),
		// 		"[SITE_NAME]" =>  $this->settings->info->site_name
		// 		),
		// 	$email_template->message);

		// 	$this->common->send_email($email_template->title,$email_template->message, 'learnmore@cerescoin.io');
		// }
				


		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("review_account" => array("general" => 1)));

		// Loads HTML page
		$this->template->loadContent("review_account/index.php", array(
			)
		);
	}

	public function restricted_group() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("groups" => 1)));

		if(!$this->user_model->check_user_in_group($this->user->info->ID, 2)) {
			$this->template->error("You are not in the User Group Friends so you cannot view this page!");
		}

		// Loads HTML page
		$this->template->loadContent("review_account/group.php", array(
			)
		);
	}

	public function restricted_admin() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("general" => 1)));

		if(!isset($this->user->info->user_role_id) || !$this->user->info->admin) {
			$this->template->error("You cannot view this page as you are not an admin!");
		}

		// Loads HTML page
		$this->template->loadContent("review_account/admin.php", array(
			)
		);
	}

	public function restricted_user() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("users" => 1)));

		if($this->user->info->username != "Admin") {
			$this->template->error("You cannot view this page as you are not the user Admin!");
		}

		// Loads HTML page
		$this->template->loadContent("review_account/user.php", array(
			)
		);
	}

	public function restricted_premium() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("premium" => 1)));

		if($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) {
			$this->template->error("You need to be a Premium Member in order to access this page!");
		}

		// Loads HTML page
		$this->template->loadContent("review_account/premium.php", array(
			)
		);
	}

}

?>