<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verify_submitted extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("admin_model");
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		// If the user does not have premium. 
		// -1 means they have unlimited premium
		if($this->settings->info->global_premium && 
			($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) ) {
			$this->session->set_flashdata("globalmsg", lang("success_29"));
			redirect(site_url("funds/plans"));
		}
	}

	public function check_status() { 
	
		// $apiURL = 'https://verifyinvestor.com/api/v1';
		// $apiURL = 'https://verifyinvestor.com/api/v1/users/135/verification_requests/90';
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$apiToken = '2d55995b8c97af4262b67cc50aa2e1a48cd1993b7dba501f32';
		$authToken = '9970cf781bc326ae';
		$apiURL = 'https://verifyinvestor-api-staging.herokuapp.com/api/v1/users/identifier/';

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_HTTPHEADER, Array("Authorization: Token 2d55995b8c97af4262b67cc50aa2e1a48cd1993b7dba501f32","Content-Type: application/json","Content-Accept: application/json"));
		curl_setopt($ch, CURLOPT_URL,$apiURL);
		$result=curl_exec($ch);
		curl_close($ch);
		var_dump(json_decode($result, true));

		$this->template->loadContent("verify_submitted/check_status.php", array(
			)
		);

	} 
	

	public function index() 
	{
		if($this->user->info->user_role == 10) {
			// $buyer_email = $this->common->nohtml($this->input->post("vi_user_id", true));

			$uridata = $_GET;
			$vi_user = (isset($uridata["vi_user_id"])) ? $uridata["vi_user_id"] : '' ;
			$regdate = date("Y-m-d");
			// $regdate = new DateTime();

			$data = array("user_role" => 7);
			$this->db->where("ID",$this->user->info->ID);
			$this->db->update("users",$data); 
			
			$data = array(				
						'user_id' => $this->user->info->ID,
						'val_submit_ts'  => $regdate,
						'val_refid'  => $vi_user,
						'status' => 1,
						'val_source' => 'VerifyInvestor'
					);
					$this->db->where("ID",$this->user->info->ID);
					$str = $this->db->insert('investors', $data);

		} else {
			redirect(site_url());
		}

		// Loads HTML page
		$this->template->loadContent("verify_submitted/index.php", array(
			)
		);
	}

	public function restricted_group() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("groups" => 1)));



		if(!$this->user_model->check_user_in_group($this->user->info->ID, 2)) {
			$this->template->error("You are not in the User Group Friends so you cannot view this page!");
		}

		// Loads HTML page
		$this->template->loadContent("verify_submitted/group.php", array(
			)
		);
	}

	public function restricted_admin() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("general" => 1)));

		if(!isset($this->user->info->user_role_id) || !$this->user->info->admin) {
			$this->template->error("You cannot view this page as you are not an admin!");
		}

		// Loads HTML page
		$this->template->loadContent("verify_submitted/admin.php", array(
			)
		);
	}

	public function restricted_user() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("users" => 1)));

		if($this->user->info->username != "Admin") {
			$this->template->error("You cannot view this page as you are not the user Admin!");
		}

		// Loads HTML page
		$this->template->loadContent("verify_submitted/user.php", array(
			)
		);
	}

	public function restricted_premium() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("premium" => 1)));

		if($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) {
			$this->template->error("You have not completed the investor verification process.");
		}

		// Loads HTML page
		$this->template->loadContent("verify_submitted/premium.php", array(
			)
		);
	}

}

?>