<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verify_webhook extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("user_model");
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		// If the user does not have premium. 
		// -1 means they have unlimited premium
		if($this->settings->info->global_premium && 
			($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) ) {
			$this->session->set_flashdata("globalmsg", lang("success_29"));
			redirect(site_url("funds/plans"));
		}
	}

	public function index() 
	{

		if($_POST['verification_request_id']) {


			$this->db->where("username",$this->user->info->ID);
			$this->db->update("ceresusers",$data); 
		
			$data = array(				
				'user_id' => $this->user->info->ID,
				'val_source' => 'VerifyInvestor',
				'first_name' => $this->user->info->first_name,
				'last_name' => $this->user->info->last_name,
				'street_address' => $this->user->info->address_1,
				'apt_address' => $this->user->info->address_2,
				'city' => $this->user->info->city,
				'state' => $this->user->info->state,
				'zipcode' => $this->user->info->zipcode,
				'country' => $this->user->info->country,
				'create_ts' => $regdate
			);
			$str = $this->db->insert('investors', $data);
	

		}
		// if(
		// 	$this->user->info->user_role == 5
		// )
		// {

		// 	$regdate = new DateTime();

		// 	$data = array("user_role" => 7);
		// 	$this->db->where("ID",$this->user->info->ID);
		// 	$this->db->update("users",$data); 

		// 	$data = array(				
		// 		'user_id' => $this->user->info->ID,
		// 		'email' => $this->user->info->email,
		// 		'first_name' => $this->user->info->first_name,
		// 		'last_name' => $this->user->info->last_name,
		// 		'street_address' => $this->user->info->address_1,
		// 		'apt_address' => $this->user->info->address_2,
		// 		'city' => $this->user->info->city,
		// 		'state' => $this->user->info->state,
		// 		'zipcode' => $this->user->info->zipcode,
		// 		'country' => $this->user->info->country,
		// 		'create_ts' => $regdate->getTimestamp()
		// 	);
		// 	$str = $this->db->insert('ceresusers', $data);
		// 	$insert_id = $this->db->insert_id();

		// 	$data = array(				
		// 				'user_id' => $insert_id,
		// 				'val_submit_ts'  => $regdate->getTimestamp(),
		// 				'status' => 1,
		// 				'val_source' => 'VerifyInvestor'
		// 			);
		// 	$str = $this->db->insert('investors', $data);

		// }
		
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("verify_webhook" => array("general" => 1)));

		// Loads HTML page
		$this->template->loadContent("verify_webhook/index.php", array(
			)
		);
	}

	public function restricted_group() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("groups" => 1)));

		if(!$this->user_model->check_user_in_group($this->user->info->ID, 2)) {
			$this->template->error("You are not in the User Group Friends so you cannot view this page!");
		}

		// Loads HTML page
		$this->template->loadContent("verify_webhook/group.php", array(
			)
		);
	}

	public function restricted_admin() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("general" => 1)));

		if(!isset($this->user->info->user_role_id) || !$this->user->info->admin) {
			$this->template->error("You cannot view this page as you are not an admin!");
		}

		// Loads HTML page
		$this->template->loadContent("verify_webhook/admin.php", array(
			)
		);
	}

	public function restricted_user() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("users" => 1)));

		if($this->user->info->username != "Admin") {
			$this->template->error("You cannot view this page as you are not the user Admin!");
		}

		// Loads HTML page
		$this->template->loadContent("verify_webhook/user.php", array(
			)
		);
	}

	public function restricted_premium() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("premium" => 1)));

		if($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) {
			$this->template->error("You need to be a Premium Member in order to access this page!");
		}

		// Loads HTML page
		$this->template->loadContent("verify_webhook/premium.php", array(
			)
		);
	}

}

?>