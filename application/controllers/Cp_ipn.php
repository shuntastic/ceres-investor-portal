<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cp_ipn extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("user_model");
		if(!$this->user->loggedin) {
			// redirect(site_url("login"));
		}

		// If the user does not have premium. 
		// -1 means they have unlimited premium
		if($this->settings->info->global_premium && 
			($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) ) {
			$this->session->set_flashdata("globalmsg", lang("success_29"));
			redirect(site_url("funds/plans"));
		}
	}

	public function index() 
	{
		// $invdata = array(				
		// 	'investor_id' => $this->user->info->ID,
		// 	'round' => 'RegD',
		// 	'transport' => 'Crypto',
		// 	'trans_refid' => $val_refid,
		// 	'trans_id' => $outdata['result']['txn_id'],
		// 	'submit_ts' => $regdate->getTimestamp(),
		// 	'amount' => $outdata['result']['amount'],
		// 	'currency' => $currency2,
		// 	'status_url' => $outdata['result']['status_url'],
		// 	'ref_amt_usd'=>floatval($currency2)
		// );
		// $this->db->where("ID",$this->user->info->ID);
		// $str = $this->db->insert('investments', $invdata);
		
		// $data = array("user_role" => 12);
		// $this->db->where("ID",$this->user->info->ID);
		// $this->db->update("users",$data); 

		// $uridata = $_GET;
		// $vi_user = (isset($uridata["vi_user_id"])) ? $uridata["vi_user_id"] : '' ;

		$cp_merchant_id = 'b9ecce4a21e589646d4d4e66eb23b796'; 
		$cp_ipn_secret = 'ed60b6983fa057328def82376442b06e'; 
		$cp_debug_email = 'learnmore@cerescoin.io'; 
	
		//These would normally be loaded from your database, the most common way is to pass the Order ID through the 'custom' POST field. 
		// $order_currency = 'USD'; 
		// $order_total = 10.00; 
	
		function errorAndDie($error_msg) { 
			global $cp_debug_email; 
			if (!empty($cp_debug_email)) { 
				$report = 'Error: '.$error_msg."\n\n"; 
				$report .= "POST Data\n\n"; 
				foreach ($_POST as $k => $v) { 
					$report .= "|$k| = |$v|\n"; 
				} 
				mail($cp_debug_email, 'CoinPayments IPN Error', $report); 
			} 
			die('IPN Error: '.$error_msg); 
		} 
	
		if (!isset($_POST['ipn_mode']) || $_POST['ipn_mode'] != 'hmac') { 
			errorAndDie('IPN Mode is not HMAC'); 
		} 
	
		if (!isset($_SERVER['HTTP_HMAC']) || empty($_SERVER['HTTP_HMAC'])) { 
			errorAndDie('No HMAC signature sent.'); 
		} 
	
		$request = file_get_contents('php://input'); 
		if ($request === FALSE || empty($request)) { 
			errorAndDie('Error reading POST data'); 
		} 
	
		if (!isset($_POST['merchant']) || $_POST['merchant'] != trim($cp_merchant_id)) { 
			errorAndDie('No or incorrect Merchant ID passed'); 
		} 
	
		$hmac = hash_hmac("sha512", $request, trim($cp_ipn_secret)); 
		if (!hash_equals($hmac, $_SERVER['HTTP_HMAC'])) { 
		//if ($hmac != $_SERVER['HTTP_HMAC']) { <-- Use this if you are running a version of PHP below 5.6.0 without the hash_equals function 
			errorAndDie('HMAC signature does not match'); 
		} 
		 
		// HMAC Signature verified at this point, load some variables. 

		$regdate = new DateTime();
		$txn_id = $_POST['txn_id']; 
		$item_name = $_POST['item_name']; 
		$item_number = $_POST['item_number']; 
		$amount1 = floatval($_POST['amount1']); 
		$amount2 = floatval($_POST['amount2']); 
		$currency1 = $_POST['currency1']; 
		$currency2 = $_POST['currency2']; 
		$status = intval($_POST['status']); 
		$status_text = $_POST['status_text']; 
	
		//depending on the API of your system, you may want to check and see if the transaction ID $txn_id has already been handled before at this point 
	
		// Check the original currency to make sure the buyer didn't change it. 
		if ($currency1 != $order_currency) { 
			errorAndDie('Original currency mismatch!'); 
		}     
		 
		// Check amount against order total 
		if ($amount1 < $order_total) { 
			errorAndDie('Amount is less than order total!'); 
		} 
	   
		if ($status >= 100 || $status == 2) { 

			// payment is complete or queued for nightly payout, success 
			$invdata = array(				
				'receive_ts' => $regdate->getTimestamp(),
				'trans_refid' => $val_refid,
				'trans_id' => $outdata['result']['txn_id']
			);
			$this->db->where("trans_id",$txn_id);
			$str = $this->db->update('investments', $invdata);

			// $query = $this->db->get();
			// if($query->num_rows()>0) {
			
			// 	$data = $query->row_array();
			// 	$invdata = array(				
			// 		'user_role' => 12
			// 	);
			// 	$value = $data['user_id'];
			// 	$this->db->where('user_id',$value);
			// 	$str = $this->db->update('users', $invdata2);
			// }
	 

		
		} else if ($status < 0) { 
			//payment error, this is usually final but payments will sometimes be reopened if there was no exchange rate conversion or with seller consent 
						
			$this->db->from('investments');
			$this->db->where('trans_id',$txn_id);
			$query = $this->db->get();

			if($query->num_rows()>0) {
			
				$data = $query->row_array();
				$value = $data['user_id'];
				$invdata = array(				
							'status' => 12
						);
				$this->db->where('user_id',$value);
				$str = $this->db->update('investors', $invdata);
			}
		 

		} else { 
			//payment is pending, you can optionally add a note to the order page 
			$this->db->from('investments');
			$this->db->where('trans_id',$txn_id);
			$query = $this->db->get();

			if($query->num_rows()>0) {
			
				$data = $query->row_array();
				$value = $data['user_id'];
				$invdata = array(				
							'status' => 11
						);
				$this->db->where('user_id',$value);
				$str = $this->db->update('investors', $invdata);
			}

		} 
		die('IPN OK'); 		
		$this->session->set_flashdata("globalmsg", 'Payment Information Submitted.');


		// Loads HTML page
		$this->template->loadContent("cp_ipn/index.php", array(
			)
		);
	}

	public function restricted_group() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("groups" => 1)));

		if(!$this->user_model->check_user_in_group($this->user->info->ID, 2)) {
			$this->template->error("You are not in the User Group Friends so you cannot view this page!");
		}

		// Loads HTML page
		$this->template->loadContent("cp_ipn/group.php", array(
			)
		);
	}

	public function restricted_admin() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("general" => 1)));

		if(!isset($this->user->info->user_role_id) || !$this->user->info->admin) {
			$this->template->error("You cannot view this page as you are not an admin!");
		}

		// Loads HTML page
		$this->template->loadContent("cp_ipn/admin.php", array(
			)
		);
	}

	public function restricted_user() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("users" => 1)));

		if($this->user->info->username != "Admin") {
			$this->template->error("You cannot view this page as you are not the user Admin!");
		}

		// Loads HTML page
		$this->template->loadContent("cp_ipn/user.php", array(
			)
		);
	}

	public function restricted_premium() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("premium" => 1)));

		if($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) {
			$this->template->error("You need to be a Premium Member in order to access this page!");
		}

		// Loads HTML page
		$this->template->loadContent("cp_ipn/premium.php", array(
			)
		);
	}

}

?>