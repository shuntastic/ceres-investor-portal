<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submit_crypto extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("home_model");
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		// If the user does not have premium. 
		// -1 means they have unlimited premium
		if($this->settings->info->global_premium && 
			($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) ) {
			$this->session->set_flashdata("globalmsg", lang("success_29"));
			redirect(site_url("funds/plans"));
		}
	}

	public function index() 
	{


		if(
			$this->user->info->user_role == 8 ||
			$this->user->info->user_role == 1
		)
		{
			//
		} else {
			redirect(base_url());
		}

		// Assigns the highlight to the sidebar link
		// $this->template->loadData("activeLink", 
		// 	array("submit_crypto" => array("general" => 1)));

		// Loads HTML page
		$this->template->loadContent("submit_crypto/index.php", array(
			)
		);
	}

	public function submit_payment() 
	{

		function coinpayments_api_call($cmd, $req = array()) { 
			$public_key = '96e44f63ab032a77c60f21b1178c38f53c5028e849960a3d06f8d4033431368e'; 
			$private_key = '9834bceF0084994bf9CfC33ECad3DE0D3DA082d6E904c36eD0b111e42f52404d'; 
			
			$req['version'] = 1; 
			$req['cmd'] = $cmd; 
			$req['key'] = $public_key; 
			$req['format'] = 'json'; //supported values are json and xml 
			 
			$post_data = http_build_query($req, '', '&'); 
			$hmac = hash_hmac('sha512', $post_data, $private_key); 
			static $ch = NULL; 
			if ($ch === NULL) { 
				$ch = curl_init('https://www.coinpayments.net/api.php'); 
				curl_setopt($ch, CURLOPT_FAILONERROR, TRUE); 
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
			} 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('HMAC: '.$hmac)); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); 
			 
			$data = curl_exec($ch);                 
			if ($data !== FALSE) { 
				if (PHP_INT_SIZE < 8 && version_compare(PHP_VERSION, '5.4.0') >= 0) { 
					// We are on 32-bit PHP, so use the bigint as string option. If you are using any API calls with Satoshis it is highly NOT recommended to use 32-bit PHP 
					$dec = json_decode($data, TRUE, 512, JSON_BIGINT_AS_STRING); 
				} else { 
					$dec = json_decode($data, TRUE); 
				} 
				if ($dec !== NULL && count($dec)) { 
					return $dec; 
				} else { 
					// If you are using PHP 5.5.0 or higher you can use json_last_error_msg() for a better error message 
					return array('error' => 'Unable to parse JSON result ('.json_last_error().')'); 
				} 
			} else { 
				return array('error' => 'cURL error: '.curl_error($ch)); 
			} 
		} 

		$buyer_email ="";
		$buyer_name ="";
		$item_name ="";
		$ipn_url ="";
		$amount ="";
		$currency1 ="";
		$currency2 ="";
		
		$buyer_email = $this->common->nohtml($this->input->post("buyer_email", true));
		$buyer_name = $this->common->nohtml($this->input->post("buyer_name", true));
		$item_name = $this->common->nohtml($this->input->post("item_name", true));
		$ipn_url = $this->common->nohtml($this->input->post("ipn_url", true));
		$amount = $this->common->nohtml($this->input->post("amount", true));
		$currency1 = $this->common->nohtml($this->input->post("currency1", true));
		$currency2 = $this->common->nohtml($this->input->post("currency2", true));


		$regdate = new DateTime();
		$val_refid = 'CRSCPT'.rand(10000000,100000000000000000);
		$val_submit = $regdate->getTimestamp();
		

		$transData = array(
			'buyer_email' => $buyer_email,
			'buyer_name' => $buyer_name,
			'item_name' => $item_name,
			'ipn_url' => $ipn_url,
			'amount' => $amount,
			'currency1' => $currency1,
			'currency2' => $currency2,
			'ipn_url' => 'https://investor.cerescoin.io/cp_ipn'
		);
		// $outdata = print_r(coinpayments_api_call('create_transaction',$transData)); 
		$outdata = coinpayments_api_call('create_transaction',$transData); 

		// Array (
		// 	[error] => ok
		// 	[result] => Array (
		// 		[amount] => 8.72988233
		// 		[txn_id] => CPCC4D4GHPVPORAUIQNAVLBPTB
		// 		[address] => 33ugxE2gX5cBU9W9VmK23t5WF9wyCnazHE
		// 		[confirms_needed] => 2
		// 		[timeout] => 97200
		// 		[status_url] => https://www.coinpayments.net/index.php?cmd=status&id=CPCC4D4GHPVPORAUIQNAVLBPTB&key=99efc807d4fec616fb735fe8a31b077d
		// 		[qrcode_url] => https://www.coinpayments.net/qrgen.php?id=CPCC4D4GHPVPORAUIQNAVLBPTB&key=99efc807d4fec616fb735fe8a31b077d ) )

		if(isset($outdata) and $outdata['error']=='ok'){
			$invdata = array(				
				'investor_id' => $this->user->info->ID,
				'round' => 'RegD',
				'transport' => 'Crypto',
				'trans_refid' => $val_refid,
				'trans_id' => $outdata['result']['txn_id'],
				'submit_ts' => $regdate->getTimestamp(),
				'amount' => $outdata['result']['amount'],
				'currency' => $currency2,
				'status_url' => $outdata['result']['status_url'],
				'ref_amt_usd'=>floatval($currency2)
			);
			$this->db->where("ID",$this->user->info->ID);
			$str = $this->db->insert('investments', $invdata);
			
			$data = array("user_role" => 14);
			$this->db->where("ID",$this->user->info->ID);
			$this->db->update("users",$data); 
			
			$this->session->set_flashdata("globalmsg", 'Payment Information Submitted.');
			// redirect(site_url("submit_crypto/payment_info_sent/".$val_refid));
			$this->template->loadContent("submit_crypto/payment_info_sent.php", array('status_url'=>$outdata['result']['status_url'],'refid'=> $val_refid
				)
			);

		} else {
			$this->session->set_flashdata("globalmsg", 'There was an error processing your payment.').print_r($outdata['result']);
			$this->template->loadContent("submit_crypto/index.php", array(
				)
			);	
		}

	}


	public function payment_info_sent($refid) 
	{
		$this->load->helper('email');
		if($this->user->info->user_role == 12) {
			// Send Email
			$email = $this->input->post("email", true);
			$email_template = $this->home_model
				->get_email_template_hook("general_message", "english");
			if($email_template->num_rows() == 0) {
				$this->template->error(lang("error_48"));
			}
			$email_template = $email_template->row();
			$message = 'Thank you for submitting your crypto payment.<br />'.
			'Here is the reference number for your transaction.<br /><br />'.
			'Transaction ID:'.$refid.'<br />';
	
			$email_template->message = $this->common->replace_keywords(array(
				"[NAME]" => $this->user->info->first_name.' '.$this->user->info->last_name,
				"[EMAIL_MESSAGE]" => $message
				),
			$email_template->message);
	
			$this->common->send_email($email_template->title,
					$email_template->message, $email);
		}	

		$this->template->loadContent("submit_crypto/payment_info_sent.php", array('refid'=>$refid
			)
		);
	}


	public function restricted_group() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("groups" => 1)));

		if(!$this->user_model->check_user_in_group($this->user->info->ID, 2)) {
			$this->template->error("You are not in the User Group Friends so you cannot view this page!");
		}

		// Loads HTML page
		$this->template->loadContent("submit_crypto/group.php", array(
			)
		);
	}

	public function restricted_admin() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("general" => 1)));

		if(!isset($this->user->info->user_role_id) || !$this->user->info->admin) {
			$this->template->error("You cannot view this page as you are not an admin!");
		}

		// Loads HTML page
		$this->template->loadContent("submit_crypto/admin.php", array(
			)
		);
	}

	public function restricted_user() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("users" => 1)));

		if($this->user->info->username != "Admin") {
			$this->template->error("You cannot view this page as you are not the user Admin!");
		}

		// Loads HTML page
		$this->template->loadContent("submit_crypto/user.php", array(
			)
		);
	}

	public function restricted_premium() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("premium" => 1)));

		if($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) {
			$this->template->error("You need to be a Premium Member in order to access this page!");
		}

		// Loads HTML page
		$this->template->loadContent("submit_crypto/premium.php", array(
			)
		);
	}

}

?>