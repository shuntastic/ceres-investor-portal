<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submit_ach extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("home_model");

		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		// If the user does not have premium. 
		// -1 means they have unlimited premium
		if($this->settings->info->global_premium && 
			($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) ) {
			$this->session->set_flashdata("globalmsg", lang("success_29"));
			redirect(site_url("funds/plans"));
		}
	}

	public function index() 
	{

		if(
			$this->user->info->user_role == 8 ||
			$this->user->info->user_role == 1
		)
		{
			//
		} else {
			redirect(base_url());
		}

		// // Assigns the highlight to the sidebar link
		// $this->template->loadData("activeLink", 
		// 	array("submit_ach" => array("general" => 1)));

		// Loads HTML page
		$this->template->loadContent("submit_ach/index.php", array(
			)
		);
	}

	public function submit_payment() 
	{

		$amount = '';
		$bankname = '';
		$routingnumber = '';

		$amount = $this->common->nohtml($this->input->post("amount", true));
		$bankname = $this->common->nohtml($this->input->post("bankname", true));
		$routingnumber = $this->common->nohtml($this->input->post("routingnumber", true));
		$regdate = new DateTime();
		$val_refid = 'CRSACH'.rand(10000000,100000000000000000);
		$val_submit = $regdate->getTimestamp();

		// $this->db->where('ID', $this->user->info->ID);
		// $query = $this->db->get();

		// if($query->num_rows()>0) {
			// $data = $query->row_array();
			// $value = $data['id'];
			$invdata = array(				
				'investor_id' => $this->user->info->ID,
				'round' => 'RegD',
				'transport' => 'ACH',
				'trans_refid' => $val_refid,
				'submit_ts' => $regdate->getTimestamp(),
				'bank_name' => $bankname,
				'bank_routing' => $routingnumber,
				'amount' => number_format($amount,2),
				'currency' => 'USD',
			);
			$this->db->where("ID",$this->user->info->ID);
			$str = $this->db->insert('investments', $invdata);
		// }


		$data = array("user_role" => 14);
		$this->db->where("ID",$this->user->info->ID);
		$this->db->update("users",$data); 

		$this->session->set_flashdata("globalmsg", 'Payment Information Saved.');
		redirect(site_url("submit_ach/payment_info_sent/".$val_refid));

	}


	public function payment_info_sent($refid) 
	{
		$this->load->helper('email');
		if($this->user->info->user_role == 12) {

			// Send Email
			$email = $this->input->post("email", true);
			$email_template = $this->home_model
				->get_email_template_hook("general_message", 'english');
			if($email_template->num_rows() == 0) {
				$this->template->error(lang("error_48"));
			}
			$email_template = $email_template->row();
			$message = 'Thank you for submitting your banking information.<br />'.
			'Use the banking information below to complete the ACH transfer.<br /><br />'.
			'Send Payment to:'.$refid.'<br />'.
			'XXXX Bank (need info)<br />'.
			'Routing Number: XXXXXXXXXX (need info)<br />'.
			'Account Number: XXXXXXXXXX (need info)<br />'.
			'Transaction ID (include this number in the memo area): '.$refid.'<br />';
	
			$email_template->message = $this->common->replace_keywords(array(
				"[NAME]" => $this->user->info->first_name.' '.$this->user->info->last_name,
				"[EMAIL_MESSAGE]" => $message
				),
			$email_template->message);
	
			$this->common->send_email($email_template->title,
					$email_template->message, $email);
		}

		$this->template->loadContent("submit_ach/payment_info_sent.php", array('refid'=>$refid
			)
		);
	}


	public function restricted_group() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("groups" => 1)));

		if(!$this->user_model->check_user_in_group($this->user->info->ID, 2)) {
			$this->template->error("You are not in the User Group Friends so you cannot view this page!");
		}

		// Loads HTML page
		$this->template->loadContent("submit_ach/group.php", array(
			)
		);
	}

	public function restricted_admin() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("general" => 1)));

		if(!isset($this->user->info->user_role_id) || !$this->user->info->admin) {
			$this->template->error("You cannot view this page as you are not an admin!");
		}

		// Loads HTML page
		$this->template->loadContent("submit_ach/admin.php", array(
			)
		);
	}

	public function restricted_user() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("users" => 1)));

		if($this->user->info->username != "Admin") {
			$this->template->error("You cannot view this page as you are not the user Admin!");
		}

		// Loads HTML page
		$this->template->loadContent("submit_ach/user.php", array(
			)
		);
	}

	public function restricted_premium() 
	{
		// Assigns the highlight to the sidebar link
		$this->template->loadData("activeLink", 
			array("restricted" => array("premium" => 1)));

		if($this->user->info->premium_time != -1 && 
				$this->user->info->premium_time < time()) {
			$this->template->error("You need to be a Premium Member in order to access this page!");
		}

		// Loads HTML page
		$this->template->loadContent("submit_ach/premium.php", array(
			)
		);
	}

}

?>