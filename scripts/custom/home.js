$(document).ready(function() {

	function showErrors(form, errors) {
		_.each(form.querySelectorAll("input[name], select[name]"), function(input) {
			showErrorsForInput(input, errors && errors[input.name]);
		});
	}

	function showErrorsForInput(input, errors) {
		var formGroup = closestParent(input.parentNode, "form-group"),
		messages = formGroup.querySelector(".messages");
		
		resetFormGroup(formGroup);
		if (errors) {
			formGroup.classList.add("has-error");
			_.each(errors, function(error) {
				addError(messages, error);
			});
		} else {
			formGroup.classList.add("has-success");
		}
	}

	function closestParent(child, className) {
		if (!child || child == document) {
			return null;
		}
		if (child.classList.contains(className)) {
			return child;
		} else {
			return closestParent(child.parentNode, className);
		}
	}

	function resetFormGroup(formGroup) {
		formGroup.classList.remove("has-error");
		formGroup.classList.remove("has-success");
		_.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
			el.parentNode.removeChild(el);
		});
	}

	function addError(messages, error) {
		var block = document.createElement("p");
		block.classList.add("help-block");
		block.classList.add("error");
		block.innerText = error;
		messages.appendChild(block);
	}


  //	PREREGISTER FORM
  if ($('form#preregister').length > 0) {
    var constraints = {
      email: {presence: true,email: true},
      first_name: {presence: true},
      last_name: {presence: true},
      address_1: { presence: true},
      city: {presence: true},
      state: {presence: true},
      zipcode: {
        presence: true,
        format: {
          pattern: '\\d{5}'
        }
      },
      country: {
        presence: true
      }
    };

    $('#preregister').submit(function(e) {
			// e.preventDefault();
			// handleFormSubmit(this);
      // var params =
      //   'a18ef3c2738f36f5?identifier=' +
      //   $('#username').val() +
      //   '&first_name=' +
      //   $('#first_name').val() +
      //   '&last_name=' +
      //   $('#last_name').val() +
      //   '&email=' +
      //   $('#email').val() +
      //   '&redirect_url=http://investor.cerescoin.io/verify_submitted';
      // window.open('http://verifyinvestor-staging.herokuapp.com/authorization/' + params);
		});
  }

  //	CHECK VERIFY STATUS
  if ($('#checkVerifyStatus').length > 0) {
		    $('#checkVerifyStatus').on('click',function(e) {
			$.post('/third_party/verifyinvestor/check_status.php', function(evt) {
				console.log('success',evt);
			})
			.done(function(evt) {
				console.log('call done',evt);
			})
			.fail(function(err) {
				console.log('call failed',err);
			});
			e.preventDefault();
		});
  }
});
