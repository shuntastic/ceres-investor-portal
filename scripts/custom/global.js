function load_notifications() 
{
	
	$.ajax({
		url: global_base_url + "home/load_notifications",
		beforeSend: function () { 
		$('#loading_spinner_notification').fadeIn(10);
		$("#ajspinner_notification").addClass("spin");
	 	},
	 	complete: function () { 
		$('#loading_spinner_notification').fadeOut(10);
		$("#ajspinner_notification").removeClass("spin");
	 	},
		data: {
		},
		success: function(msg) {
			$('#notifications-scroll').html(msg);
		}

	});
	console.log("Done");
}

function load_notifications_unread() 
{
	$.ajax({
		url: global_base_url + "home/load_notifications_unread",
		beforeSend: function () { 
		$('#loading_spinner_notification').fadeIn(10);
		$("#ajspinner_notification").addClass("spin");
	 	},
	 	complete: function () { 
		$('#loading_spinner_notification').fadeOut(10);
		$("#ajspinner_notification").removeClass("spin");
	 	},
		data: {
		},
		success: function(msg) {
			$('#notifications-scroll').html(msg);
			return false;
		}

	});
	console.log("Done");
}


function load_notification_url(id) 
{
	window.location.href= global_base_url + "home/load_notification/" + id;
	return;
}

function checkVerifyInvestor() 
{
	$.ajax({
		type: "POST",
		url: global_base_url+'third_party/verifyinvestor/check_status.php',
		// data: data,
		success: function(evt){
			console.log('call made',evt);
		},
	  });
}

$(document).ready(function() {

	$('.dropdown-menu #noti-click-unread').click(function(e) {
	    e.stopPropagation();
	});
	if(navigator.appVersion.indexOf("Win")!=-1) {
		$('#notifications-scroll').niceScroll({touchbehavior: false, zindex: 9999999999});
	}
	if($('#checkVerifyStatus').length > 0) {
		$('#checkVerifyStatus').click(checkVerifyInvestor);
	}
	
	if($('#verifyInvestorLaunch').length > 0) {
		$('#launchVerifyInvestor').on('click',function(e) {
			var params =
			'9550a26eb47eb718?identifier=' +
			$('#username').val() +
			'&first_name=' +
			$('#first_name').val() +
			'&last_name=' +
			$('#last_name').val() +
			'&email=' +
			$('#email').val() +
			'&redirect_url=https://investor.cerescoin.io/verify_submitted';
			window.open('http://verifyinvestor.herokuapp.com/authorization/' + params);
			
			e.preventDefault();
		});		
	}
});